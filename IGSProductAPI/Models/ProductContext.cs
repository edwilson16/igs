﻿using Microsoft.EntityFrameworkCore;

namespace IGSProductAPI.Models
{
    /// <summary>
    /// Database context for Products
    /// </summary>
    public class ProductContext : DbContext
    {
        public ProductContext(DbContextOptions<ProductContext> options)
            : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
    }
}