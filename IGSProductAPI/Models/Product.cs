﻿namespace IGSProductAPI.Models
{
    /// <summary>
    /// Online marketplace product
    /// </summary>
    public class Product
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
    }
}