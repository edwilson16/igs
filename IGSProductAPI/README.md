﻿﻿﻿# IGS Product API

This is a RESTful API for the IGS online marketplace

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* Visual Studio
* Docker Desktop
* Postman
* Port 5000 available

### Installing

* Clone the git repository (git clone https://edwilson16@bitbucket.org/edwilson16/igs.git)
* Start Docker Desktop
* Start Visual Studio
* Open IGSProductAPI.sln in Visual Studio
* The database is an in memory Entity Framework database so there is no need to install anything for the data source. (data is seeded in Program.cs)

### Running

In Visual Studio press F5 or click the 'Docker' button to run the API and the API should start to run in a Docker container.

Your browser should open http://localhost:5000/v1/products which lists all products in the database.

You can visit http://localhost:5000/swagger/ to view the Swagger documentation for the API.

## Running the tests

* Start Postman
* Open tests.postman and run the tests in collection runner - all 37 test should pass

Note: If you run the tests a second time there will be failures - The API needs to be restarted in Visual Studio to reset the database in order for all tests to pass. Please see Suggestions section for ideas on how to improve this.

## Assumptions

1. There was some contradiction between the instructions and the tests (e.g. the instructions said to make endpoints including GET /products but the tests were configured for GET /v1/products). An assumption was made that the tests should pass unedited so the API was built to satisfy the tests rather than follow the instructions explicitly.

## Suggestions (in no particular order)

1. If products are known by their ID e.g. 1 for {"id":1,"name":"Lavender heart","price":"9.25"}, then the product code should be moved to another property in Product so it can be replied upon so that the database id can increment safely and also ensure uniqueness (This would allow the 'Get single product' test to pass multiple test runs. When 'Create a single product' runs the first time it creates a product with id 4 and 'Get single product' passes. A later test deletes the product with id 4 but the second run of 'Create a single product' creates a product with id 5 so the 'Get single product' test fails as it expects a 4)
2. Delete action test should perhaps return a 204 response rather than a 200 with a empty body but had to do this to satisfy the test
3. Bulk actions could be added for getting, adding, updating, deleting multiple products
4. Add an endpoint to get product(s) by name
5. Add an endpoint to get a list of products in a price range
6. The 'Update a single product' test only passes a name which creates some ambiguity as to what to do with its price but its clear from a later test that the price must remain untouched for it to pass. Could add endpoints to update the name and the price separately or alternatively insist on passing all values which are all updated. As it stands it would be impossible to remove a name or price.
7. Depending on the clients using the API it may be a good idea to insist on JSON requests rather than allowing form-data.
8. Improve handling of the price e.g. make it a proper number in the database in case VAT/shipping calculations are required in the future. Also need to consider localization and multi-currency.

## Next Steps (in no particular order)

 1. secure with SSL
 2. configure CORS depending on the clients that will use the API
 3. secure the endpoints with authentication (especially the Create, Read, Update and Delete operations)
 4. replace the in memory database to a proper secure database with persistence
 5. put in place more thorough error handling, reporting and logging
 6. add protection for over posting
 7. apply company coding standards and get peer reviewed
 8. consider any disaster recovery requirements and database backup
 9. consider performance requirements and if data cache or CDN is required
 10. set up CI pipeline
 11. configure environments, deploy and test
 12. add unit testing
 13. plan go-live

## Authors

**Ed Wilson**
