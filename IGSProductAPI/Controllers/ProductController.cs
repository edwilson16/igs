﻿using System.Threading.Tasks;
using IGSProductAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IGSProductAPI.Controllers
{
    /// <summary>
    /// Controller for single Product actions - Create, Read, Update, Delete
    /// </summary>
    [Route("v1/product")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ProductContext _context;

        /// <summary>
        /// set the database context for working with the products
        /// </summary>
        /// <param name="context"></param>
        public ProductController(ProductContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get a product
        /// </summary>
        /// <param name="product_id">the product's id</param>
        /// <returns></returns>
        [HttpGet("{product_id}")]
        public async Task<ActionResult<Product>> GetProduct(long product_id)
        {
            var product = await _context.Products.FindAsync(product_id);

            //invalid product - return 404
            if (product == null)
            {
                return NotFound();
            }

            return product;
        }

        /// <summary>
        /// Create a product
        /// </summary>
        /// <param name="product">The product</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Product>> PostProduct([FromForm]Product product)
        {
            //create the product and save to database
            _context.Products.Add(product);
            await _context.SaveChangesAsync();

            //return the created product
            return RedirectToAction("GetProduct", new { product_id = product.Id });
        }

        /// <summary>
        /// Update the product
        /// </summary>
        /// <param name="product_id">the product's id</param>
        /// <param name="updatedProduct">the updated product</param>
        /// <returns></returns>
        [HttpPut("{product_id}")]
        public async Task<IActionResult> PutProduct(long product_id, [FromForm]Product updatedProduct)
        {
            //find the product in the database
            var product = await _context.Products.FindAsync(product_id);

            //invalid product - return 404
            if (product == null)
            {
                return NotFound();
            }

            //had to make an assumption here that if no Name or Price are not provided it means that they
            //shouldn't change rather than being nulled
            if (!string.IsNullOrWhiteSpace(updatedProduct.Name))
            {
                //set product name if provided with one
                product.Name = updatedProduct.Name;
            }

            if (!string.IsNullOrWhiteSpace(updatedProduct.Price))
            {
                //set product price if provided with one
                product.Price = updatedProduct.Price;
            }

            //save the product
            _context.Entry(product).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            //return the updated product
            return RedirectToAction("GetProduct", new { product_id = product.Id });
        }

        /// <summary>
        /// Delete a product
        /// </summary>
        /// <param name="product_id">the product's id</param>
        /// <returns></returns>
        [HttpDelete("{product_id}")]
        public async Task<IActionResult> DeleteProduct(long product_id)
        {
            //find the product in the database
            var product = await _context.Products.FindAsync(product_id);

            //invalid product - return 404
            if (product == null)
            {
                return NotFound();
            }

            //remove product from the database
            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}