﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IGSProductAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IGSProductAPI.Controllers
{
    /// <summary>
    /// Controller for actions on all products - GET
    /// </summary>
    [Route("v1/products")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ProductContext _context;

        /// <summary>
        /// set the database context for working with the products
        /// </summary>
        /// <param name="context"></param>
        public ProductsController(ProductContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get all the products
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> GetProducts()
        {
            return await _context.Products.ToListAsync();
        }
    }
}